---
layout: markdown_page
title: "INF 201 - Using Terraform to manage the GitLab.com infrastructure"
---

## Aim

This course is for people who need to manage the GitLab.com infrastructure
using [Terraform](https://www.terraform.io). You will learn how our
configuration is structured with a particular focus for cattle nodes.

## Requirements

* Basic Terraform knowledge.

## Steps

1. [ ] [Set up the Terraform environment](https://gitlab.com/gitlab-com/gitlab-com-infrastructure#how-can-i-use-this)
1. [ ] Watch [this video](https://drive.google.com/open?id=0BzamLYNnSQa_cjN5NGtaRnpyRXc) about the creation of a new canary environment. It covers almost aspect of the way we use Terraform.
1. [ ] [Learn how to work with cattle](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/terraform/working-with-cattle.md).
