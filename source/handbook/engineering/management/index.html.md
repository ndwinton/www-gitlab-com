---
layout: markdown_page
title: "Engineering Management"
---

## How Engineering Management Works at GitLab

At GitLab, we promote two paths for leadership in Engineering. While there is a
healthy degree of overlap between these two ideas, it is helpful and efficient
for us to specialize training and responsibility for each of:

- **Technical leadership**, as represented by [Staff and higher-level
  developers](/job-families/engineering/developer/#staff-developer).
- **Professional leadership**, as represented by [Engineering
  management](/job-families/engineering/engineering-management/).

While technical leadership tends to come naturally to software engineers,
professional leadership can be more difficult to master. This page will serve as
a training resource and operational guide for current and future managers.

## On this page
{:.no_toc}

- TOC
{:toc}

## General leadership principles

All Engineering Managers should follow the [general leadership
principles](/handbook/leadership) set out in the handbook. In particular, it is
not uncommon for Engineering Managers to struggle with one or more of the
following areas, so we recommend you review them carefully and discuss your
confidence with your manager:

- [1-1s](/handbook/leadership/1-1)
- [Providing regular feedback](handbook/leadership/#giving-performance-feedback)
- [Dealing with underperformance](/handbook/underperformance/)

## Hiring

- **Hiring is a priority.** GitLab is a fast-growing company, and paying
  attention to hiring is one of the highest-leverage activities a manager can
  perform. As long as there are vacancies on your team, hiring should be your top
  priority.
- **Hiring is your responsibility.** You may rely on other members of your team
  to help you evaluate potential candidates. We also have many people at GitLab,
  most notably in Recruiting, who will help you through the process. But it is
  always your responsibility to make timely, high-quality hires - speak up if
  you feel like anything is preventing you from accomplishing this goal.
- **Hiring is about making the team better.** It is easy to try to find
  candidates who can merely perform the functions of the role, and more
  difficult to find people who make the team better while they do it.
- **Hiring is hard.** It often involves making difficult decisions, and learning
  to do it well can take years. In times of rapid growth, it may consume 50% or
  more of your time. Please use every resource available to you, from
  more experienced hiring managers in the company to the many resources
  [available in our handbook](/handbook/hiring/interviewing/).

## Career Coaching for Developers

Outside of [hiring](#hiring), the best way to improve the strengths of your team
is to practice career development coaching with your developers. While they will
not all become Staff Developers or Engineering Managers at GitLab, identifying
their career goals and proactively working towards them is the most effective
way to help everyone improve. In addition to the company-wide notes on [career
mapping and
development](/handbook/people-operations/learning-and-development/#career-mapping-and-development),
here are some important considerations for the Engineering team:

- **Technical or Professional Leadership?** This is the first section on our
  [career development
  page](/handbook/engineering/career-development/#individual-contribution-vs-management)
  for a reason. For Senior Developers, this is the most important question to
  ask about their career goals, as it will determine where they go next. For
  anyone who hasn't yet made Senior, this is less important - they need to
  make Senior first - but it's always a good idea to start talking about this
  fork in our career paths.
- **Assess together.** Once you know where your Developer wants to go with their
  careers, it's time to assess their strengths and weaknesses for the position.
  This is best done by reviewing the [responsibilities and
  requirements](/job-families/engineering/developer) of the job they're
  interested in. If you both assess it separately, you can compare and contrast
  your results.
- **Create a behavior-based plan.** Once you understand the Developer's
  strengths and weaknesses, help them understand how they can improve by
  focusing on behaviors they can change. Be careful to avoid tasks like "run
  this meeting" or "complete this project," and be careful to avoid personality
  traits like "be less angry" or "be more assertive." "When you realize you're
  in an argument, take a breath and figure out how to move forward
  constructively" is an example of a good behavioral action.
- **Delegate and give feedback.** When you are in agreement about the behavioral
  action plan, start looking for opportunities to delegate appropriate tasks to
  the Developer. A Senior looking to move to Staff may benefit from taking some
  architectural conversations off of your plate, while one looking for a Manager
  job will learn a lot from tackling some project management initiatives.
- **Keep the plan updated.** It's important to remember that this is a coaching
  plan, _not_ a promotion plan. There is never any guarantee that the plan you
  created will result in a promotion if the Developer "checks all the boxes,"
  and you should feel free to add to or remove from the plan as they grow and
  you learn more about them. If and when you feel that they are ready for
  promotion, please follow the [normal promotion
  process](/handbook/people-operations/promotions-transfers/).

## Team retrospectives

In addition to the [public function-wide
retrospective](handbook/engineering/workflow/#retrospective), each Engineering
team should be in the practice of holding their own retrospectives. The results
of these retrospectives should then inform the function-wide retrospective for
any given release. [More information on running effective retrospectives is
available here](/handbook/engineering/management/team-retrospectives).
