---
layout: markdown_page
title: "Configuration Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Configuration Team

The configuration team is responsible for developing features of GitLab that
relate to the "Configuration" part of the DevOps lifecycle. This team maintains 
our integrations with Kubernetes including the [Auto DevOps](/auto-devops) feature
set.

The configuration team is also responsible for building out new Feature Flag capabilities for GitLab.